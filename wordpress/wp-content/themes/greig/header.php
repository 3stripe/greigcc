<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<!-- <a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', '_s' ); ?></a> -->

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<!-- <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> -->
			<?php else : ?>
				<!-- <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p> -->
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<!-- <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p> -->
			<?php
			endif; ?>
		</div><!-- .site-branding -->

		<header class="tc pv4 pt5-ns">
		  <a href="/">
			  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-greig.png" class="w4"
			   alt="James Greig">
			   
		</header>
			
		<nav class="tc sans-serif">
			<div class="">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => ''  ) ); ?>
			    <!-- <a class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" href="#">Start here</a>
			    <a class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" href="#">Articles</a>
			    <a class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" href="#">Books &amp; Courses</a>
			    <a class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" href="#">Design</a>
			    <a class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" href="#">About</a>
			    <a class="link dim dark-gray f6 f5-ns dib mr3 mr4-ns" href="#">Contact</a> -->
			  </div>
		</nav>
	</header><!-- #masthead -->

	<div id="content" class="site-content">