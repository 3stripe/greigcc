<?php
$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
// $context['newsletter_signup'] = TimberHelper::function_wrapper( 'mc4wp_get_form', array('516') );
$mc4wp_args = array(
	'id' => '516',
	array( 'element_class' => 'mt5 mh3 mh5-m mh6-l mb6 pa2 pa4-ns pb4 mw700' ),
	false
);
$context['newsletter_signup'] = TimberHelper::function_wrapper( 'mc4wp_show_form', $mc4wp_args);

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
