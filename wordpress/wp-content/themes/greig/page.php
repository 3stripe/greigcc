<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['newsletter_signup'] = TimberHelper::function_wrapper( 'mc4wp_get_form', array('516') );
Timber::render(array('page-' . $post->post_name . '.twig', 'page.twig'), $context);
