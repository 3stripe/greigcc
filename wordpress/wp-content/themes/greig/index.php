<?php
$context = Timber::get_context();
$context['pagination'] = Timber::get_pagination();
$context['posts'] = Timber::get_posts();
$mc4wp_args = array(
	'id' => '516',
	array( 'element_class' => 'mt5 mh3 mh5-m mh6-l pa2 pa4-ns pb4 mw700' ),
	false
);
$context['newsletter_signup'] = TimberHelper::function_wrapper( 'mc4wp_show_form', $mc4wp_args);
$templates = array( 'index.twig' );
$context['title'] = 'Articles';

if ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	$context['category_description'] = category_description();
	$context['categories'] = Timber::get_terms('category');
}

if ( is_home() ) {
	array_unshift( $templates, 'home.twig' );
}

Timber::render( $templates, $context );
