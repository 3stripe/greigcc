<?php

// if ( ! function_exists( 'greig_setup' ) ): function greig_setup() {{

// =============================================
// Wordpress config
// =============================================
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7 );
remove_action('wp_print_styles', 'print_emoji_styles' );
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'wp_oembed_add_host_js');
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

// Disable JSON API
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

// Remove p tags yo
// remove_filter('term_description','wpautop');

// Allow HTML in category descriptions, via  http://wordpress.org/extend/plugins/allow-html-in-category-descriptions/
// Disables Kses only for textarea saves
foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
	remove_filter($filter, 'wp_filter_kses');
}
// Disables Kses only for textarea admin displays
foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
	remove_filter($filter, 'wp_kses_data');
}


// =============================================
// Remove trailing zero from month number in permalinks
// =============================================
// add_filter( 'the_permalink', 't5_strip_leading_zeros_in_url', 5, 2 );
// add_filter( 'get_permalink', 't5_strip_leading_zeros_in_url', 5, 2 );
// add_filter( 'get_the_permalink', 't5_strip_leading_zeros_in_url', 5, 2 );
// add_filter( 'month_link', 't5_strip_leading_zeros_in_url', 5, 2 );
// // add_filter( 'day_link',   't5_strip_leading_zeros_in_url' );
// function t5_strip_leading_zeros_in_url( $url )
// {
//     // no pretty permalinks
//     if ( ! $GLOBALS['wp_rewrite']->get_month_permastruct() )
//     {
//         return $url;
//     }
//     return str_replace( '/0', '/', $url );
// }

// =============================================
// Enable Timber templating plugin
// =============================================
add_filter('timber_context', 'add_to_context');

function add_to_context($data){
    $data['menu'] = new TimberMenu();
    return $data;
}

// =============================================
// Menus
// =============================================

function register_greig_menus() {
  register_nav_menu('nav',__( 'nav' ));
}
add_action( 'init', 'register_greig_menus' );

// =============================================
// Sidebars
// =============================================

// register_sidebar( array(
// 'name' => 'Footer Sidebar 1',
// 'id' => 'footer-sidebar-1',
// 'description' => 'Appears in the footer area',
// 'before_widget' => '<aside id="%1$s" class="widget %2$s">',
// 'after_widget' => '</aside>',
// 'before_title' => '<h3 class="widget-title">',
// 'after_title' => '</h3>',
// ) );

// =============================================
// Remove <p> tags from around images and iframes
// =============================================

function filter_ptags_on_images($content){
	$images = '/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s';
	$video = '/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU';
	return preg_replace(array($images,$video), '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

// =============================================
// Style MC4WP forms with Tachyons
// =============================================
// Now added using new 'element_class' parameter
// add_filter( 'mc4wp_form_css_classes', function( $classes ) {
// 	$classes[] = 'mh3 mh5-ns pa2 pa4-ns pb4 mw700';
// 	return $classes;
// });

// =============================================
// File me
// =============================================
define('CONCATENATE_SCRIPTS', false);

function greigstyle() {
    // Register the style like this for a theme:
    wp_register_style( 'greig', get_template_directory_uri().'/style.css');

    // enqueue the stule
    wp_enqueue_style( 'greig' );
}
add_action( 'wp_enqueue_scripts', 'greigstyle' );


// }} endif;
