<?php
/*
 * Template Name: Fullscreen Landing Page
 * Description: A Page Template with a fullscreen background image.
 */
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['newsletter_signup'] = TimberHelper::function_wrapper( 'mc4wp_get_form', array('516') );
Timber::render(array('page-landing.twig', 'page.twig'), $context);
